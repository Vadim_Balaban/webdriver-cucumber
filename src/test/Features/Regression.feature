Feature: Working with posts page
  Scenario: Create simple post
      Given opening new post page
      When specifying post title
      And specifying post body
      And clicking publish button
      Then Post should be created

  Scenario: Delete All Posts
      Given opening all posts page
      When selecting 'title' checkbox
      And selecting 'move to trash' from dropdown menu
      And clicking 'Apply' button
      Then 'No posts found.' should be appeared on the page