import core.BaseTest;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

/**
 * Created by vbalaban on 3/6/2015.
 */


@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        format = { "json:target/cucumber.json" },
        features = "src/test/Features/")
public class CukeRunnerTest extends BaseTest{

        @BeforeClass
        public static void setUp(){
                loginPage.Open();
                driver.manage().window().maximize();
                loginPage.ProvideUserName();
                loginPage.ProvidePassword();
                loginPage.ClickOnLoginButton();
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        }

        @AfterClass
        public static void tearDown(){

        }

}
