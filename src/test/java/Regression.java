import core.ApplicationSettings;
import core.BaseTest;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pageObjects.PostViewPage;
import pageObjects.PostsPage;

import static core.WebdriverExtension.isElementContainsText;

/**
 * Created by vbalaban on 3/6/2015.
 */

public class Regression extends BaseTest {

//    @Before
//    public void setUp() throws Exception {
//        loginPage.Open();
//        driver.manage().window().maximize();
//        loginPage.ProvideUserName();
//        loginPage.ProvidePassword();
//        loginPage.ClickOnLoginButton();
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//    }

    @Given("^opening new post page$")
    public void opening_new_post_page(){
        newPostPage.Open();
    }

    @When("^specifying post title$")
    public void specifying_post_title() {
        newPostPage.ProvidePostTitle("Test text for Presentation");
    }

    @And("^specifying post body$")
    public void specifying_post_body() {
        newPostPage.ProvidePostBody("Test Body text for presentation");
    }

    @And("^clicking publish button$")
    public void clicking_publish_button() {
        newPostPage.ClickOnPublishButon();
    }

    @Then("^Post should be created$")
    public void Post_should_be_created() {
        newPostPage.ClickOnViewPostLink();
        Assert.assertTrue(isElementContainsText(PostViewPage.PostTitle, "Test text for Presentation"));
        Assert.assertTrue(isElementContainsText(PostViewPage.PostBody, "Test Body text for presentation"));
    }

    @Given("^opening all posts page$")
    public void opening_all_posts_page(){
        driver.navigate().to(ApplicationSettings.baseUrl);
        PostsPage.AllPosts.Select();
    }

    @When("^selecting 'title' checkbox$")
    public void selecting_title_checkbox(){
        PostsPage.TitleCheckbox.Check();
    }

    @And("^selecting 'move to trash' from dropdown menu$")
    public void selecting_move_to_trash_from_dropdown_menu() {
        PostsPage.ActionDropDownMenu.SelectDropDownValue("trash");
    }

    @And("^clicking 'Apply' button$")
    public void clicking_Apply_button() {
        PostsPage.ApplyButton.Click();
    }

    @Then("^'No posts found.' should be appeared on the page$")
    public void _No_posts_found_should_be_appeared_on_the_page() {
        Assert.fail();
        //Assert.assertTrue(isTextPresentOnPage("No posts found."));
    }

    @Given("^edit post page$")
    public void edit_post_page() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @When("^selecting$")
    public void selecting() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @Then("^should be failed$")
    public void should_be_failed() throws Throwable {
        Assert.fail();
    }

    @When("^selecting several posts$")
    public void selecting_several_posts() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @Then("^should be passed$")
    public void should_be_passed() throws Throwable {
    }
}
